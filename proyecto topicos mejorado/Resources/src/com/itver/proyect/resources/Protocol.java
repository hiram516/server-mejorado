
package com.itver.proyect.resources;

import java.io.Serializable;


public enum Protocol implements Serializable{
    LOGGIN, REGISTER, PLAY, REQUEST_ACEPTED, REQUEST_FAILED ;
}
